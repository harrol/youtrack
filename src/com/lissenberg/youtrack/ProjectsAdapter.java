package com.lissenberg.youtrack;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ProjectsAdapter extends ArrayAdapter<Project> {

    public ProjectsAdapter(Context context, List<Project> objects) {
        super(context, R.layout.project_view, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View projectView = inflater.inflate(R.layout.project_view, parent, false);
        TextView s = (TextView) projectView.findViewById(R.id.shortName);
        TextView n = (TextView) projectView.findViewById(R.id.name);
        TextView d = (TextView) projectView.findViewById(R.id.description);
        Project p = getItem(position);
        s.setText(p.getShortName());
        n.setText(p.getName());
        d.setText(p.getDescription());
        return projectView;
    }
}
