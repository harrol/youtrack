package com.lissenberg.youtrack;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class YoutrackClient {

    String location;
    AndroidHttpClient client;
    HttpContext context;
    CookieStore cookieStore;

    public YoutrackClient() {
        this("http://youtrack.jetbrains.com/rest");
    }

    public YoutrackClient(String location) {
        this.location = location;
        client = AndroidHttpClient.newInstance("Youtrack for Android");
        context = new BasicHttpContext();
        cookieStore = new BasicCookieStore();
        context.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
    }

    public boolean logon(String user, String password) {
        // POST /user/login
        // txt
        // login=username&password=password
        try {
            return new LogonTask().execute(user, password).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

    public List<Project> getProjects() {

        try {
            return new ProjectsTask().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String newLocation) throws IllegalArgumentException {

        try {
            if (new URLCheckTask().execute(newLocation).get()) {
                this.location = newLocation;
                if(this.location.endsWith("/")) {
                    this.location = this.location.substring(0, this.location.length());
                }
            }
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
        }
        if(!this.location.equals(newLocation)) {
            throw new IllegalArgumentException("Invalid url/could not connect: " + newLocation);
        }
    }

    private class URLCheckTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                URLConnection urlConnection = url.openConnection();
                urlConnection.connect();
                return true;
            } catch (IOException e) {
                return false;
            }
        }
    }

    private class ProjectsTask extends AsyncTask<Void, Void, List<Project>> {
        // <projects><project versions, name, shortName, description...>
        @Override
        protected List<Project> doInBackground(Void... voids) {
            ProjectsHandler handler = new ProjectsHandler();
            HttpGet get = new HttpGet(location + "/project/all");
            try {
                String result = client.execute(get, new BasicResponseHandler(), context);

                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser parser = factory.newSAXParser();
                parser.parse(new InputSource(new StringReader(result)), handler);

                Log.d("projects", result);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }
            return handler.getProjectList();
        }
    }

    private class LogonTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            cookieStore.clear();
            HttpPost post = new HttpPost(location + "/user/login");
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("login", strings[0]));
            params.add(new BasicNameValuePair("password", strings[1]));
            Boolean result = false;
            try {
                post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                result = client.execute(post, new ResponseHandler<Boolean>() {
                    @Override
                    public Boolean handleResponse(HttpResponse httpResponse) throws IOException {
                        if (200 == httpResponse.getStatusLine().getStatusCode()) {
                            return true;
                        } else {
                            Log.w("youtrack", "Logon failed with code:  + httpResponse.getStatusLine())");
                            return false;
                        }
                    }
                }, context);
            } catch (IOException e) {
                Log.e("logon", e.getMessage());
            }
            return result;
        }
    }
}
