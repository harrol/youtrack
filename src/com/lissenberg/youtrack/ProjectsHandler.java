package com.lissenberg.youtrack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class ProjectsHandler extends DefaultHandler {

    private List<Project> projectList = new ArrayList<Project>();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if("project".equals(localName)) {
            projectList.add(new Project(attributes.getValue("shortName"), attributes.getValue("name"),attributes.getValue("description")));
        }
    }

    public List<Project> getProjectList() {
        return projectList;
    }
}
