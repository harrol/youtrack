package com.lissenberg.youtrack;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

public class ProjectsActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    public static final String TAG = "youtrack";

    private EditText location;
    private Button logon, projects;
    private ListView projectsList;
    private YoutrackClient client = new YoutrackClient();
    private ArrayAdapter<Project> projectAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        projects = (Button) findViewById(R.id.projects);
        projectsList = (ListView) findViewById(R.id.listView);

        findViewById(R.id.logon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = ((EditText) findViewById(R.id.user)).getText().toString();
                String password = ((EditText) findViewById(R.id.password)).getText().toString();
                Log.d(TAG, "Logon user " + user);
                if (client.logon(user, password)) {
                    projects.setEnabled(true);
                } else {
                    projects.setEnabled(false);
                }
            }
        });

        projects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Project> found = client.getProjects();
                Log.d("projects", found.toString());
                projectAdapter = new ProjectsAdapter(getApplicationContext(), found);
                projectsList.setAdapter(projectAdapter);
            }
        });

        location = (EditText) findViewById(R.id.location);
        location.setText(client.getLocation());
        location.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(!hasFocus) {
                    String newLocation = location.getText().toString();
                    Log.d(TAG, "New location: " + newLocation);

                    try {
                        client.setLocation(newLocation);
                    } catch (IllegalArgumentException e) {
                        location.setText(client.getLocation());
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        });
    }
}
